var data = [
    {
        age: 'young',
        dose: 1,
        sex: 'W',
        hospitalization: false,
        doseType: 'Moderna',
        posX: 53.92,
        posY: 14.20
    },
    {
        age: 'middle',
        dose: 1,
        sex: 'M',
        hospitalization: true,
        doseType: 'Moderna',
        posX: 54.32,
        posY: 14.20
    }
]